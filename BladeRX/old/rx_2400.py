#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Rx 2400
# Generated: Fri Mar 17 18:52:50 2017
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import math
import osmosdr
import time


class rx_2400(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Rx 2400")

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 10
        self.baud_rate = baud_rate = 2425
        self.samp_rate_tx = samp_rate_tx = 1500000
        self.samp_rate = samp_rate = int(baud_rate * sps)
        self.rx_vga_gain = rx_vga_gain = 20
        self.rx_lna_gain = rx_lna_gain = 6
        self.freq = freq = 446.5e6

        ##################################################
        # Blocks
        ##################################################
        self.osmosdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "bladerf=0" )
        self.osmosdr_source_0.set_sample_rate(samp_rate_tx)
        self.osmosdr_source_0.set_center_freq(freq, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_dc_offset_mode(1, 0)
        self.osmosdr_source_0.set_iq_balance_mode(1, 0)
        self.osmosdr_source_0.set_gain_mode(False, 0)
        self.osmosdr_source_0.set_gain(rx_lna_gain, 0)
        self.osmosdr_source_0.set_if_gain(0, 0)
        self.osmosdr_source_0.set_bb_gain(rx_vga_gain, 0)
        self.osmosdr_source_0.set_antenna("", 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)
          
        self.low_pass_filter_0_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	1, 96000, baud_rate, 0.5*baud_rate, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, 1500000, 32000, 8000, firdes.WIN_HAMMING, 6.76))
        self.fir_filter_xxx_0 = filter.fir_filter_fff(16, (firdes.low_pass(1.0, baud_rate*sps, baud_rate, 0.25*baud_rate)))
        self.fir_filter_xxx_0.declare_sample_delay(0)
        self.digital_pfb_clock_sync_xxx_0 = digital.pfb_clock_sync_fff(sps, 2*3.14159265/85, (firdes.low_pass(1.0, baud_rate*sps, baud_rate, 0.25*baud_rate)), 32, 16, 1.5, 1)
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_char*1, "/usr/share/adafruit/webide/repositories/bladerf/BladeRX/_out.bin", False)
        self.blocks_file_sink_0_0.set_unbuffered(True)
        self.blocks_add_const_vxx_0 = blocks.add_const_vff((0, ))
        self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(8)
        self.analog_pwr_squelch_xx_0 = analog.pwr_squelch_cc(-70, 1e-4, 0, True)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_pwr_squelch_xx_0, 0), (self.low_pass_filter_0, 0))    
        self.connect((self.analog_quadrature_demod_cf_0, 0), (self.blocks_add_const_vxx_0, 0))    
        self.connect((self.blocks_add_const_vxx_0, 0), (self.low_pass_filter_0_0, 0))    
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.blocks_file_sink_0_0, 0))    
        self.connect((self.digital_pfb_clock_sync_xxx_0, 0), (self.fir_filter_xxx_0, 0))    
        self.connect((self.fir_filter_xxx_0, 0), (self.digital_binary_slicer_fb_0, 0))    
        self.connect((self.low_pass_filter_0, 0), (self.analog_quadrature_demod_cf_0, 0))    
        self.connect((self.low_pass_filter_0_0, 0), (self.digital_pfb_clock_sync_xxx_0, 0))    
        self.connect((self.osmosdr_source_0, 0), (self.analog_pwr_squelch_xx_0, 0))    

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_samp_rate(int(self.baud_rate * self.sps))
        self.fir_filter_xxx_0.set_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.digital_pfb_clock_sync_xxx_0.update_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        self.set_samp_rate(int(self.baud_rate * self.sps))
        self.fir_filter_xxx_0.set_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.digital_pfb_clock_sync_xxx_0.update_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(1, 96000, self.baud_rate, 0.5*self.baud_rate, firdes.WIN_HAMMING, 6.76))

    def get_samp_rate_tx(self):
        return self.samp_rate_tx

    def set_samp_rate_tx(self, samp_rate_tx):
        self.samp_rate_tx = samp_rate_tx
        self.osmosdr_source_0.set_sample_rate(self.samp_rate_tx)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_rx_vga_gain(self):
        return self.rx_vga_gain

    def set_rx_vga_gain(self, rx_vga_gain):
        self.rx_vga_gain = rx_vga_gain
        self.osmosdr_source_0.set_bb_gain(self.rx_vga_gain, 0)

    def get_rx_lna_gain(self):
        return self.rx_lna_gain

    def set_rx_lna_gain(self, rx_lna_gain):
        self.rx_lna_gain = rx_lna_gain
        self.osmosdr_source_0.set_gain(self.rx_lna_gain, 0)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_source_0.set_center_freq(self.freq, 0)


def main(top_block_cls=rx_2400, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
