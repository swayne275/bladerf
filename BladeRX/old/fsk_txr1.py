#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Fsk Txr1
# Generated: Fri Mar 17 15:28:43 2017
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import math


class fsk_txr1(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Fsk Txr1")

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 10
        self.baud_rate = baud_rate = 9400
        self.waveshape = waveshape = 12000
        self.txvga2 = txvga2 = 20
        self.txvga1 = txvga1 = -9
        self.samp_rate_tx = samp_rate_tx = 1500000
        self.samp_rate = samp_rate = int(baud_rate * sps)
        self.quad_gain = quad_gain = 5
        self.freq = freq = 446.5e6

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccc(
                interpolation=samp_rate_tx,
                decimation=samp_rate,
                taps=None,
                fractional_bw=None,
        )
        self.low_pass_filter_0_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	1, 96000, baud_rate, 0.5*baud_rate, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, 1500000, 4*waveshape, waveshape, firdes.WIN_HAMMING, 6.76))
        self.fir_filter_xxx_0 = filter.fir_filter_fff(16, (firdes.low_pass(1.0, baud_rate*sps, baud_rate, 0.25*baud_rate)))
        self.fir_filter_xxx_0.declare_sample_delay(0)
        self.digital_pfb_clock_sync_xxx_0 = digital.pfb_clock_sync_fff(sps, 2*3.14159265/85, (firdes.low_pass(1.0, baud_rate*sps, baud_rate, 0.25*baud_rate)), 32, 16, 1.5, 1)
        self.digital_gfsk_mod_0 = digital.gfsk_mod(
        	samples_per_symbol=sps,
        	sensitivity=1.0,
        	bt=1,
        	verbose=False,
        	log=False,
        )
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((0.8, ))
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, "/usr/share/adafruit/webide/repositories/bladerf/BladeRX/_send.bin", True)
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_char*1, "/usr/share/adafruit/webide/repositories/bladerf/BladeRX/_out.bin", False)
        self.blocks_file_sink_0_0.set_unbuffered(True)
        self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(quad_gain)
        self.analog_pwr_squelch_xx_0 = analog.pwr_squelch_cc(-70, 1e-4, 0, True)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_pwr_squelch_xx_0, 0), (self.low_pass_filter_0, 0))    
        self.connect((self.analog_quadrature_demod_cf_0, 0), (self.low_pass_filter_0_0, 0))    
        self.connect((self.blocks_file_source_0, 0), (self.digital_gfsk_mod_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.analog_pwr_squelch_xx_0, 0))    
        self.connect((self.blocks_throttle_0, 0), (self.analog_quadrature_demod_cf_0, 0))    
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.blocks_file_sink_0_0, 0))    
        self.connect((self.digital_gfsk_mod_0, 0), (self.rational_resampler_xxx_0, 0))    
        self.connect((self.digital_pfb_clock_sync_xxx_0, 0), (self.fir_filter_xxx_0, 0))    
        self.connect((self.fir_filter_xxx_0, 0), (self.digital_binary_slicer_fb_0, 0))    
        self.connect((self.low_pass_filter_0, 0), (self.blocks_throttle_0, 0))    
        self.connect((self.low_pass_filter_0_0, 0), (self.digital_pfb_clock_sync_xxx_0, 0))    
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0))    

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_samp_rate(int(self.baud_rate * self.sps))
        self.digital_pfb_clock_sync_xxx_0.update_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.fir_filter_xxx_0.set_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        self.set_samp_rate(int(self.baud_rate * self.sps))
        self.digital_pfb_clock_sync_xxx_0.update_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.fir_filter_xxx_0.set_taps((firdes.low_pass(1.0, self.baud_rate*self.sps, self.baud_rate, 0.25*self.baud_rate)))
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(1, 96000, self.baud_rate, 0.5*self.baud_rate, firdes.WIN_HAMMING, 6.76))

    def get_waveshape(self):
        return self.waveshape

    def set_waveshape(self, waveshape):
        self.waveshape = waveshape
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, 1500000, 4*self.waveshape, self.waveshape, firdes.WIN_HAMMING, 6.76))

    def get_txvga2(self):
        return self.txvga2

    def set_txvga2(self, txvga2):
        self.txvga2 = txvga2

    def get_txvga1(self):
        return self.txvga1

    def set_txvga1(self, txvga1):
        self.txvga1 = txvga1

    def get_samp_rate_tx(self):
        return self.samp_rate_tx

    def set_samp_rate_tx(self, samp_rate_tx):
        self.samp_rate_tx = samp_rate_tx

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_quad_gain(self):
        return self.quad_gain

    def set_quad_gain(self, quad_gain):
        self.quad_gain = quad_gain
        self.analog_quadrature_demod_cf_0.set_gain(self.quad_gain)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq


def main(top_block_cls=fsk_txr1, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
